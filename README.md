# pvc_processing

Monitor pvc usage in the ml.cern.ch clusters.
Delete unused pvcs.

`git clone https://gitlab.cern.ch/ai-ml/pvc_processing.git`

## Build

`cd pvc_processing/docker`

`docker build . -f Dockerfile --network=host -t gitlab-registry.cern.ch/ai-ml/pvc_processing/pvc_processing:vX`

`docker push gitlab-registry.cern.ch/ai-ml/pvc_processing/pvc_processing:vX`

## Deploy

Add the actual secret files instead of placeholders in `secrets-files` directory.

`cd ..`

`kubectl create secret generic openrc-secret --from-file=secrets-files/openrc.sh`

`kubectl create secret generic kubeconfig-secret --from-file=secrets-files/kubeconfig`

`kubectl apply -f crds/pvc-processing-role-binding.yaml`

`kubectl apply -f crds/pvc-processing-cron-job.yaml`
