import kubernetes
from kubernetes import client, config
import pandas as pd
import smtplib
from email.message import EmailMessage
import subprocess

def get_manila_shares():
    manila_shares = {}
    
    manila_lines = subprocess.check_output(['manila', \
                                            'list']).decode('UTF-8').split('\n')

    for manila_line in manila_lines[3:-2]:
        manila_id = manila_line.split('|')[1][1:-1]
        pv_name = manila_line.split('|')[2][1:-1]
        manila_shares[pv_name] = manila_id
    
    return manila_shares

def get_pvs(api):
    response_pv = api.list_persistent_volume()
    pvs = []
    
    for pv in response_pv.items:
        pvs.append(pv.metadata.name)

    return pvs

def get_pvcs(api):
    response_pvc = api.list_persistent_volume_claim_for_all_namespaces()
    pvcs = {}

    for pvc in response_pvc.items:
        pvcs[pvc.spec.volume_name] = pvc

    return pvcs

def aggregate_pvcs(pvs, manila_shares, pvcs):
    df_res = pd.DataFrame(columns=['namespace', 'pvc', 'pv', 'manila'])

    for pv in pvs:
        res = {}
        res['pv'] = pv
        res['manila'] = manila_shares[pv] if pv in manila_shares else None
        res['pvc'] = pvcs[pv].metadata.name if pv in pvcs else None
        res['namespace'] = pvcs[pv].metadata.namespace if pv in pvcs else None
        df_res = df_res.append(res, ignore_index=True)

    return df_res

def send_report(df_pvc, shares_threshold=100):
    total_shares = df_pvc.loc[~df_pvc['manila'].isnull()]
    hanging_pvcs = df_pvc.loc[df_pvc['manila'].isnull() & \
                              ~df_pvc['pvc'].isnull()]
    hanging_shares = df_pvc.loc[~df_pvc['manila'].isnull() & \
                                df_pvc['pvc'].isnull()]

    message_text = '\nManila shares in the cluster: ' + str(len(total_shares)) + \
                    '\nHanging shares (no pvcs): ' + str(len(hanging_shares)) + \
                    '\nHanging pvcs (no shares): ' + str(len(hanging_pvcs))

    msg = EmailMessage()
    msg['From'] = "dejan.golubovic@cern.ch"
    msg['To'] = "dejan.golubovic@cern.ch"

    if len(total_shares) > shares_threshold:
        message_headline = 'ALERT, total shares over threshold\n'
        msg['Subject'] = 'Shares - ALERT'
    elif len(hanging_shares) > 0:
        message_headline = 'ALERT, hanging shares\n'
        msg['Subject'] = 'Shares - ALERT'
    else:
        message_headline = 'All good!\n'
        msg['Subject'] = 'Shares - ALL GOOD'

    print(message_headline + message_text)
    msg.set_content(message_headline + message_text)

    mailserver = smtplib.SMTP()
    mailserver.connect('smtp.cern.ch')

    mailserver.starttls()
    #mailserver.login(svcacc, svcacc_password)
    #mailserver.send_message(msg)

config.load_incluster_config()
print('Loaded incluster_config')

v1 = client.CoreV1Api()
print('Using Client')

manila_shares = get_manila_shares()
print('Number of manila shares in the project: ' + str(len(manila_shares)))

pvs = get_pvs(v1)
print('Number of pvs: ' + str(len(pvs)))

pvcs = get_pvcs(v1)
print('Number of pvcs: ' + str(len(pvcs)))

df_pvc = aggregate_pvcs(pvs, manila_shares, pvcs)
print('Aggregated')

df_pvc.to_csv('shares.csv')
print('Saved as csv')

send_report(df_pvc)
print('Report sent')

print('Monitoring Script Completed')
