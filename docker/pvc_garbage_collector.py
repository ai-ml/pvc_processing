"""
Purpose:
- Delete all KALE (marshal-pvc) and Notebook (workspace) PVCs in the user namespaces
- Delete a PVC, its corresponding PV, and a manila share

Conditions for deletion:
- PVC name contains 'marshal-pvc' OR 'workspace'
- AND
- PVC is not mounted by any pod

This code DOES NOT:
- Delete hanging PVs
- Delete hanging manila shares
"""

import subprocess
import sys
import time
import argparse
import kubernetes
from kubernetes import client, config

def get_namespaces(api):
    system_namespaces = ['admkst', 'argocd', 'cert-manager', 'default', 'istio-system', \
                        'knative-serving', 'kube-node-lease', 'kube-public', \
                        'kube-system', 'kubeflow', 'magnum-tiller', 'monit']

    all_namespaces = api.list_namespace()

    return [namespace.metadata.name for namespace in all_namespaces.items \
            if not namespace.metadata.name in system_namespaces]

def get_filtered_namespaced_pvcs(api, namespace):
    # Get all pvcs which contain 'marshal-pvc' in their name and are not mounted
    pvcs = list()
    print(namespace)
    ns_pvcs = api.list_namespaced_persistent_volume_claim(namespace)

    for pvc in ns_pvcs.items:
        if not 'marshal-pvc' in pvc.metadata.name and \
           not 'workspace' in pvc.metadata.name:
            continue

        pvc_describe_lines = subprocess.check_output(['kubectl', '-n', namespace, \
            'describe', 'pvc', pvc.metadata.name]).decode('UTF-8').split('\n')
        
        for line in pvc_describe_lines:
            if not 'Used By' in line and not 'Mounted By' in line:
                continue

            line = ' '.join(line.split())
            mount_pod = line.split(': ')[1]

            if mount_pod == '<none>':
                pvcs.append(pvc)

            break
    
    return pvcs

def get_pv_names(api):
    response_pv = api.list_persistent_volume()
    pv_names = set()
    
    for pv in response_pv.items:
        pv_names.add(pv.metadata.name)

    return pv_names

def get_manila_shares():
    manila_shares = {}
    
    manila_lines = subprocess.check_output(['manila', \
                                            'list']).decode('UTF-8').split('\n')

    for manila_line in manila_lines[3:-2]:
        manila_id = manila_line.split('|')[1][1:-1]
        pv_name = manila_line.split('|')[2][1:-1]
        manila_shares[pv_name] = manila_id
    
    return manila_shares

def delete_pvc_pv_manila(api, namespace, pvc, pv_names, manila_shares):
    api.delete_namespaced_persistent_volume_claim(pvc.metadata.name, namespace)
    print('PVC ' + pvc.metadata.name + ' in namespace ' + namespace + ' marked for deletion')

    pv_name = pvc.spec.volume_name

    if pv_name in pv_names:
        api.delete_persistent_volume(pv_name)
        print('PV ' + pv_name + ' marked for deletion')
    else:
        print('PV not found, PV not deleted.')

    if pv_name in manila_shares:
        subprocess.Popen(['manila', 'delete', manila_shares[pv_name]])
        print('manila share ' + manila_shares[pv_name] + ' marked for deletion')
    else:
        print('Manila share not found, manila share not deleted.')

def process_namespaced_pvcs(api, namespace, pv_names, manila_shares):
    pvcs = get_filtered_namespaced_pvcs(api, namespace)
        
    for pvc in pvcs:
        print('pvc_name: ' + pvc.metadata.name)

        delete_pvc_pv_manila(api, namespace, pvc, pv_names, manila_shares)

def main():
    config.load_incluster_config()
    v1 = client.CoreV1Api()
    
    namespaces = get_namespaces(v1)
    manila_shares = get_manila_shares()
    pv_names = get_pv_names(v1)

    for namespace in namespaces:
        process_namespaced_pvcs(v1, namespace, pv_names, manila_shares)

if __name__ == "__main__":
    main()
